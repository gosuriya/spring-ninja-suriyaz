package com.suriya.springninjasuriya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringNinjaSuriyaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringNinjaSuriyaApplication.class, args);
	}

}
